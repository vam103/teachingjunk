# teachingjunk

Random files generated while teaching. If they are of use for you, then great. If you have a suggestion to make them better, cool I will update them.

[installR-AGRC1023.bat](https://gitlab.com/vam103/teachingjunk/-/blob/master/installR-AGRC1023.bat) is a batch file to install R and RStudio in nearly the new versions. (**needs administrator rights**)

[installR-AGRC1023.ps1](https://gitlab.com/vam103/teachingjunk/-/blob/master/installR-AGRC1023.ps1) is a powershell verison of the file above and requests administrator rights

[extractNotes.R](https://gitlab.com/vam103/teachingjunk/-/blob/master/extractNotes.R) is an example of how to extract records from a long string with Date: as a separator and then extract the next two dates. First version uses base R, the second uses stringr and is perhaps more straightforward.
