REM Make temporary directory
mkdir %TEMP%\AGRC1023Download\

REM Transferring files
bitsadmin /transfer AGRC1023Download /download /priority foreground https://mirror.aarnet.edu.au/pub/CRAN/bin/windows/base/R-4.1.0-win.exe %TEMP%\AGRC1023Download\R-4.1.0-win.exe https://download1.rstudio.org/desktop/windows/RStudio-1.4.1717.exe %TEMP%\AGRC1023Download\RStudio-1.4.1717.exe

REM Installing R and RStudio Silently
%TEMP%\AGRC1023Download\R-4.1.0-win.exe /VERYSILENT
%TEMP%\AGRC1023Download\RStudio-1.4.1717.exe /S

REM Cleanup
rmdir /Q /S %TEMP%\AGRC1023Download\
