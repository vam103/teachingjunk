New-Item -Path $env:TEMP -Name "AGRC1023Download" -ItemType Directory -Force

$path = $env:Temp + "\AGRC1023Download"

Import-Module BitsTransfer

$dest1 = $path + "\R-4.1.0-win.exe"
$dest2 = $path + "\RStudio-1.4.1717.exe"

Start-BitsTransfer -Source "https://mirror.aarnet.edu.au/pub/CRAN/bin/windows/base/R-4.1.0-win.exe" -Destination $dest1
Start-BitsTransfer -Source "https://download1.rstudio.org/desktop/windows/RStudio-1.4.1717.exe" -Destination $dest2

start-process -verb runAs $dest1 -argumentlist "/VERYSILENT"
start-process -verb runAs $dest2 -argumentlist "/S"

Remove-Item $path -Recurse
